public class Dragonstone.Registry.GopherTypeRegistry : Object {
	
	public List<Dragonstone.Registry.GopherTypeRegistryEntry> entrys = new List<Dragonstone.Registry.GopherTypeRegistryEntry>();
	
	public GopherTypeRegistry.default_configuration(){
		//fast
		add(new GopherTypeRegistryEntry('i',null,".",GopherTypeRegistryContentHint.TEXT));
		//standardized
		add(new GopherTypeRegistryEntry('0',"text/*"));
		add(new GopherTypeRegistryEntry('1',"text/gopher"));
		add(new GopherTypeRegistryEntry('2',null,"CCSO://{host}:{port}/{selector}"));
		add(new GopherTypeRegistryEntry('3',null,".",GopherTypeRegistryContentHint.ERROR));
		add(new GopherTypeRegistryEntry('4',"text/x-hex"));
		add(new GopherTypeRegistryEntry('5',"application/octet-stream").make_mimetype_suggestion());
		add(new GopherTypeRegistryEntry('6',null));
		add(new GopherTypeRegistryEntry('7',"text/gopher",null,GopherTypeRegistryContentHint.SEARCH));
		add(new GopherTypeRegistryEntry('8',null,"telnet://{host}:{port}"));
		add(new GopherTypeRegistryEntry('9',"application/octet-stream").make_mimetype_suggestion());
		add(new GopherTypeRegistryEntry('g',"image/gif"));
		add(new GopherTypeRegistryEntry('I',"image/*"));
		add(new GopherTypeRegistryEntry('T',null,"telnet://{host}:{port}"));
		//conventions
		add(new GopherTypeRegistryEntry('h',"text/html"));
		add(new GopherTypeRegistryEntry('p',"image/png"));
		add(new GopherTypeRegistryEntry('P',"application/pdf"));
		add(new GopherTypeRegistryEntry('s',"audio/*"));
	}
	
	public Dragonstone.Registry.GopherTypeRegistryEntry? get_entry_by_gophertype(unichar gophertype){
		foreach(Dragonstone.Registry.GopherTypeRegistryEntry entry in entrys){
			if (entry.gophertype == gophertype){
				return entry;
			}
		}
		return null;
	}
	
	public void add(Dragonstone.Registry.GopherTypeRegistryEntry entry){
		entrys.append(entry);
	}
	
}

public class Dragonstone.Registry.GopherTypeRegistryEntry {
	public unichar gophertype { get; protected set; }
	public string? mimetype { get; protected set; }
	public string uri_template { get; protected set; }
	public bool mimeyte_is_suggestion { get; protected set; }
	public GopherTypeRegistryContentHint hint { get; protected set; }
	
	public GopherTypeRegistryEntry(unichar gophertype, string? mimetype = null, string? uri_template = null, GopherTypeRegistryContentHint hint = GopherTypeRegistryContentHint.LINK){
		this.gophertype = gophertype;
		this.mimetype = mimetype;
		this.hint = hint;
		if (mimetype != null) {
			this.mimeyte_is_suggestion = mimetype.has_suffix("*");
		} else {
			this.mimeyte_is_suggestion = true;
		}
		if (uri_template != null){
			this.uri_template = uri_template;
		} else {
			this.uri_template = "gopher://{host}:{port}/{type}{selector}";
		}
	}
	
	public Dragonstone.Registry.GopherTypeRegistryEntry make_mimetype_suggestion(){
		this.mimeyte_is_suggestion = true;
		return this;
	}
	
	public string get_uri(string host, string port, string selector, string? query = null){
		if (selector.has_prefix("URL:")) {
			return selector.substring(4);
		}
		var uri = uri_template;
		uri = uri.replace("{host}",host);
		uri = uri.replace("{port}",port);
		uri = uri.replace("{type}",@"$gophertype");
		uri = uri.replace("{selector}",selector);
		if( query != null ){
			uri = uri+"\t"+(query.replace("\t","%09"));
		}
		return uri;
	}
}

public enum Dragonstone.Registry.GopherTypeRegistryContentHint {
	TEXT,
	ERROR,
	LINK,
	SEARCH;
}
