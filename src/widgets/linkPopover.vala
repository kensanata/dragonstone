public interface Dragonstone.Widget.LinkPopover : Gtk.Popover {
	public abstract void use_uri(string uri);
}

// an implemntation of this can be found in the linkbutton file
